const {defaults} = require('jest-config');

module.exports = {
  transform: {
    '^.+\\.(js|jsx)?$': 'babel-jest',
    "\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$": "<rootDir>/__mocks__/fileTransformer.js"
  },
  moduleFileExtensions: [...defaults.moduleFileExtensions, 'ts', 'tsx'],
  moduleNameMapper: {
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$': '<rootDir>/__mocks__/fileMock.js',
    '\\.(css|less)$': 'identity-obj-proxy',
    // '\\.(css|less)$': '<rootDir>/__mocks__/styleMock.js'
  },
  transformIgnorePatterns: ['<rootDir>/node_modules/'],
  verbose: true,
  setupFiles: [
    'raf/polyfill',
    '<rootDir>/src/tests/setupTests.js',
  ],
  snapshotSerializers: [
    'enzyme-to-json/serializer'
  ],
};