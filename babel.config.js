module.exports = api => {
    const isTest = api.env('test');

    const defaultConfig = {
        presets: [
            ['@babel/preset-env', {
                // development: true,
                targets: {
                    node: 'current',
                },
                "debug": false,
                "targets": {
                    "browsers": [
                        "last 3 versions"
                    ]
                }
            }],
            ['@babel/preset-react', {
                development: true,
                targets: {
                    node: 'current',
                },
                "debug": false,
                "targets": {
                    "browsers": [
                        "last 3 versions"
                    ]
                }
            }],
        ],
        plugins: [
            '@babel/plugin-proposal-class-properties',
            '@babel/plugin-proposal-object-rest-spread',
            '@babel/plugin-syntax-class-properties',
            'react-hot-loader/babel',
        ],
        env: {
            production: {
                presets: ['minify']
            }
        },
        
    }

    const testConfig = {
        presets: [
            ['@babel/preset-env', {
                // development: true,
                targets: {
                    node: 'current',
                },
                "debug": false,
                "targets": {
                    "browsers": [
                        "last 3 versions"
                    ]
                }
            }],
            ['@babel/preset-react', {
                development: true,
                targets: {
                    node: 'current',
                },
                "debug": false,
                "targets": {
                    "browsers": [
                        "last 3 versions"
                    ]
                }
            }],
        ],
        plugins: [
            '@babel/plugin-proposal-class-properties',
            '@babel/plugin-proposal-object-rest-spread',
            '@babel/plugin-syntax-class-properties',
            'react-hot-loader/babel',
        ],
        env: {
            production: {
                presets: ['minify']
            }
        },
    }

    return isTest ? testConfig : defaultConfig;
}