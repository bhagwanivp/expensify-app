import { hot } from 'react-hot-loader/root';
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import 'normalize.css/normalize.css'; // reset inbuilt CSS of browsers

import AppRouter from './routers/AppRouter'; // main app router
import 'react-dates/lib/css/_datepicker.css'; // react datepicker library CSS
import 'react-dates/initialize';    // fix for date picker 'theme not defined' error
import './styles/styles.scss'; // main app CSS

import configureStore from './store/configureStore';
import './firebase/firebase'; // set up database

import { startSetExpenses } from './actions/expenses';

const appRoot = document.getElementById('app'); // app injection path
const store = configureStore();                 // redux store config

const renderApp = (Component) => {
    render(
        <Provider store={store} >
            <Component />
        </Provider>,
        appRoot
    );
}

hot(render(<div>Loading...</div>, appRoot));

store.dispatch(startSetExpenses()).then(() => {
    hot(renderApp(AppRouter)); // app render command
});