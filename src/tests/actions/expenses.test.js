import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import database from "../../firebase/firebase";

import {
	startAddExpense,
	addExpense,
	editExpense,
	removeExpense,
	setExpenses,
	startSetExpenses,
	startRemoveExpense,
	startEditExpense
} from "../../actions/expenses";
import expenses from "../fixtures/expenses";

const createMockStore = configureMockStore([thunk]);

beforeEach(done => {
	const expensesData = {};
	expenses.forEach(({ id, description, note, amount, createdAt }) => {
		expensesData[id] = { description, note, amount, createdAt };
	});
	database
		.ref("expenses")
		.set(expensesData)
		.then(() => done());
});

test("should generate custom add expense action object", () => {
	const action = addExpense(expenses[2]);
	expect(action).toEqual({
		type: "ADD_EXPENSE",
		expense: expenses[2]
	});
});

test("should add expense to database and store", done => {
	const store = createMockStore({});
	const expenseData = {
		description: "Mouse",
		amount: 3000,
		note: "This one is better",
		createdAt: 1000
	};

	store
		.dispatch(startAddExpense(expenseData))
		.then(() => {
			const actions = store.getActions();
			expect(actions[0]).toEqual({
				type: "ADD_EXPENSE",
				expense: {
					id: expect.any(String),
					...expenseData
				}
			});

			return database
				.ref(`expenses/${actions[0].expense.id}`)
				.once("value");
		})
		.then(snapshot => {
			expect(snapshot.val()).toEqual(expenseData);
			done();
		});
});

test("should REMOVE expense from database and store", done => {
	const store = createMockStore({});
	const idToRemove = expenses[2].id;

	store
		.dispatch(startRemoveExpense({ id: idToRemove }))
		.then(() => {
			const actions = store.getActions();

			expect(actions[0]).toEqual({
				type: "REMOVE_EXPENSE",
				id: idToRemove
			});

			return database.ref(`expenses/${idToRemove}`).once("value");
		})
		.then(snapshot => {
			expect(snapshot.val()).toBeFalsy();
			done();
		});
});

test("should add DEFAULT expense to database and store", done => {
	const store = createMockStore({});
	const defaultExpenseData = {
		createdAt: 0,
		description: "",
		amount: 0,
		note: ""
	};

	store
		.dispatch(startAddExpense({}))
		.then(() => {
			const actions = store.getActions();

			expect(actions[0]).toEqual({
				type: "ADD_EXPENSE",
				expense: {
					id: expect.any(String),
					...defaultExpenseData
				}
			});

			return database
				.ref(`expenses/${actions[0].expense.id}`)
				.once("value");
		})
		.then(snapshot => {
			expect(snapshot.val()).toEqual(defaultExpenseData);
			done();
		});
});

test("should generate EDIT expense action object", () => {
	const expenseData = {
		description: "abcd",
		note: "dummy expense"
	};
	const action = editExpense("123abc", expenseData);

	expect(action).toEqual({
		type: "EDIT_EXPENSE",
		id: "123abc",
		updates: expenseData
	});
});

test("should EDIT expense in database and store", done => {
	const store = createMockStore({});
	const id = expenses[0].id;
	const updates = { amount: 21405 };

	store
		.dispatch(startEditExpense(id, updates))
		.then(() => {
			const actions = store.getActions();
			expect(actions[0]).toEqual({
				type: "EDIT_EXPENSE",
				id,
				updates
			});

			return database.ref(`expenses/${id}`).once("value");
		})
		.then(snapshot => {
			expect(snapshot.val().amount).toBe(updates.amount);
			done();
		});
});

test("should generate REMOVE expense action object", () => {
	const expenseData = {
		id: "123abc"
	};
	const action = removeExpense(expenseData);
	expect(action).toEqual({
		type: "REMOVE_EXPENSE",
		...expenseData
	});
});

test("should setup SET EXPENSE action object with data", () => {
	const action = setExpenses(expenses);
	expect(action).toEqual({
		type: "SET_EXPENSES",
		expenses
	});
});

test("should fetch the expenses from database", done => {
	const store = createMockStore({});

	store.dispatch(startSetExpenses()).then(() => {
		const actions = store.getActions();
		expect(actions[0]).toEqual({
			type: "SET_EXPENSES",
			expenses
		});

		done();
	});
});
