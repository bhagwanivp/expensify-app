import moment from "moment";

export default [
	{
		id: "1",
		description: "water bill",
		amount: 5000,
		note: "",
		createdAt: moment().startOf("year").valueOf()
	},
	{
		id: "2",
		description: "rent",
		amount: 150000,
		note: "",
		createdAt: moment().startOf("month").valueOf()
	},
	{
		id: "3",
		description: "gas bill",
		amount: 2000,
		note: "",
		createdAt: moment().startOf("week").valueOf()
	}
];
