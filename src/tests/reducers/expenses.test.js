import expensesReducer from '../../reducers/expenses';
import defaultExpenses from '../fixtures/expenses';

const initActionObj = { 
    type: '@@INIT' 
};

test('should set default state', () => {
    const state = expensesReducer(undefined, initActionObj);
    const expectedResult = [];
    expect(state).toEqual(expectedResult);
});

test('should set passed state', () => {
    const state = expensesReducer(defaultExpenses, initActionObj);
    const expectedResult = defaultExpenses;
    expect(state).toEqual(expectedResult);
});

test('should remove expense by ID', () => {
    const action = {
        type: 'REMOVE_EXPENSE',
        id: defaultExpenses[1].id,
    }
    const state = expensesReducer(defaultExpenses, action);
    const expectedResult = [ defaultExpenses[0], defaultExpenses[2] ];
    expect(state).toEqual(expectedResult);
});

test('should NOT remove expense if ID is not found', () => {
    const action = {
        type: 'REMOVE_EXPENSE',
        id: '12345',
    };
    const state = expensesReducer(defaultExpenses, action);
    const expectedResult = defaultExpenses;
    expect(state).toEqual(expectedResult);
});

test('should add a new expense', () => {
    const newExpense = {
        id: '109',
        description: 'Laptop',
        note: '',
        createdAt: 20000,
        amount: 29500
    };
    const action = {
        type: 'ADD_EXPENSE',
        expense: newExpense,
    }
    const state = expensesReducer(defaultExpenses, action);
    const expectedResult = [ ...defaultExpenses, newExpense ];
    expect(state).toEqual(expectedResult);
});

test('should edit an existing expense by ID', () => {
    const updates = {
        amount: 122000
    };
    const action = {
        type: 'EDIT_EXPENSE',
        id: defaultExpenses[1].id,
        updates: updates,
    }
    const state = expensesReducer(defaultExpenses, action);
    const expectedResult = { ...defaultExpenses[1], ...updates };
    expect(state[1]).toEqual(expectedResult);
});

test('should NOT edit an existing expense if ID is not found', () => {
    const updates = {
        amount: 122000
    };
    const action = {
        type: 'EDIT_EXPENSE',
        id: '12345',
        updates: updates,
    }
    const state = expensesReducer(defaultExpenses, action);
    const expectedResult = defaultExpenses;
    expect(state).toEqual(expectedResult);
});

test('should SET expenses', () => {
    const action = {
        type: 'SET_EXPENSES',
        expenses: [defaultExpenses[1]]
    };
    const state = expensesReducer(defaultExpenses, action);
    expect(state).toEqual([defaultExpenses[1]]);
});