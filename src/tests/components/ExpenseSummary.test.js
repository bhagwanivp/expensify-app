import React from 'react';
import { shallow } from 'enzyme';

import ExpenseSummary from '../../components/ExpenseSummary';
import defaultExpenses from '../fixtures/expenses';

test('should correctly render ExpenseSummary with 1 expense', () => {
    const wrapper = shallow(<ExpenseSummary expenses={[defaultExpenses[1]]} allExpensesCount={10} />);
    expect(wrapper).toMatchSnapshot();
});

test('should correctly render ExpenseSummary with many expense', () => {
    const wrapper = shallow(<ExpenseSummary expenses={defaultExpenses} allExpensesCount={3} />);
    expect(wrapper).toMatchSnapshot();
});