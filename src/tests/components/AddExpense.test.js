import React from 'react';
import { shallow } from 'enzyme';

import { AddExpense } from '../../components/AddExpense';
import defaultExpenses from '../fixtures/expenses';

let startAddExpense, history, wrapper;

beforeEach(() => {
    startAddExpense = jest.fn();
    history = {
        push: jest.fn()
    };
    wrapper = shallow(<AddExpense startAddExpense={startAddExpense} history={history} />);
});

test('should render AddExpense form correctly', () => {
    expect(wrapper).toMatchSnapshot();
});

test('should handle onSubmit', () => {
    wrapper.find('ExpenseForm').prop('onSubmit')(defaultExpenses[1]);
    expect(startAddExpense).toHaveBeenLastCalledWith(defaultExpenses[1]);
    expect(history.push).toHaveBeenLastCalledWith('/');
});