import React from "react";
import { shallow } from "enzyme";

import { EditExpense } from "../../components/EditExpense";
import defaultExpenses from "../fixtures/expenses";

let startEditExpense, startRemoveExpense, history, wrapper;

beforeEach(() => {
	startEditExpense = jest.fn();
	startRemoveExpense = jest.fn();
	history = {
		push: jest.fn()
	};
	wrapper = shallow(
		<EditExpense
			startEditExpense={startEditExpense}
			startRemoveExpense={startRemoveExpense}
			history={history}
			expense={defaultExpenses[2]}
		/>
	);
});

test("should render EditExpense form correctly", () => {
	expect(wrapper).toMatchSnapshot();
});

test("should handle editExpense", () => {
	wrapper.find("ExpenseForm").prop("onSubmit")(defaultExpenses[2]);
	expect(startEditExpense).toHaveBeenLastCalledWith(
		defaultExpenses[2].id,
		defaultExpenses[2]
	);
	expect(history.push).toHaveBeenLastCalledWith("/");
});

test("should handle startRemoveExpense", () => {
	wrapper.find("button").simulate("click");
	expect(startRemoveExpense).toHaveBeenLastCalledWith({
		id: defaultExpenses[2].id
	});
	expect(history.push).toHaveBeenLastCalledWith("/");
});
