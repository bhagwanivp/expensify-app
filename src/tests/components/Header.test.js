import React from "react";
import { shallow } from "enzyme";
// import ShallowRenderer from 'react-test-renderer/shallow';

import Header from "../../components/Header";

// react-test-renderer implementation
// test('should render Header correctly', () => {
//     const renderer = new ShallowRenderer();
//     renderer.render(<Header />);
//     const result = renderer.getRenderOutput();
//     expect(result).toMatchSnapshot();
// });

test("should render Header correctly", () => {
	const wrapper = shallow(<Header />);
	// expect(wrapper.find('h1').text()).toBe('Expensify Track Your Expenses');
	expect(wrapper).toMatchSnapshot();
});
