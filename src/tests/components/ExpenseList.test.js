import React from 'react';
import { shallow } from 'enzyme';

import { ExpenseList } from '../../components/ExpenseList';
import defaultExpenses from '../fixtures/expenses';

test('should render ExpenseList with expenses', () => {
    const wrapper = shallow(<ExpenseList expenses={defaultExpenses} filters={{}} />);
    expect(wrapper).toMatchSnapshot();
});

test('should render empty ExpenseList with default message', () => {
    const wrapper = shallow(<ExpenseList expenses={[]} filters={{}} />);
    expect(wrapper).toMatchSnapshot();
});