import React from 'react';
// import moment from 'moment';
import { shallow } from 'enzyme';

import ExpenseForm from '../../components/ExpenseForm';
import defaultExpenses from '../fixtures/expenses';

test('should render an empty ExpenseForm', () => {
    const wrapper = shallow(<ExpenseForm />);
    expect(wrapper).toMatchSnapshot();
});

test('should render ExpenseForm correctly with passed expense', () => {
    const wrapper = shallow(<ExpenseForm expense={defaultExpenses[1]} />);
    expect(wrapper).toMatchSnapshot();
});

test('should render error for invalid ExpenseForm submission', () => {
    const wrapper = shallow(<ExpenseForm />);
    expect(wrapper).toMatchSnapshot();
    wrapper.find('form').simulate('submit', {
        preventDefault: () => {}
    });
    expect(wrapper.state('error').length).toBeGreaterThan(0);
    expect(wrapper).toMatchSnapshot();
});

test('should set description on input change', () => {
    const wrapper = shallow(<ExpenseForm />);
    const newValue = 'New description';
    wrapper.find('input').at(0).simulate('change', {
        target: { value: newValue }
    });
    expect(wrapper.state('description')).toBe(newValue);
});

test('should set note on textarea change', () => {
    const wrapper = shallow(<ExpenseForm />);
    const newValue = 'New note';
    wrapper.find('textarea').simulate('change', {
        target: { value: newValue }
    });
    expect(wrapper.state('note')).toBe(newValue);
});

test('should set amount on input change when value is valid', () => {
    const wrapper = shallow(<ExpenseForm />);
    const newValue = '12.50';
    wrapper.find('input').at(1).simulate('change', {
        target: { value: newValue }
    });
    expect(wrapper.state('amount')).toBe(newValue);
});

test('should NOT set amount on input change when value is invalid', () => {
    const wrapper = shallow(<ExpenseForm />);
    const newValue = '12.501';
    wrapper.find('input').at(1).simulate('change', {
        target: { value: newValue }
    });
    expect(wrapper.state('amount')).toBe('');
});

test('should call onSubmit prop for valid form submission', () => {
    const onSubmitSpy = jest.fn();
    const wrapper = shallow(<ExpenseForm expense={defaultExpenses[1]} onSubmit={onSubmitSpy} />);
    wrapper.find('form').simulate('submit', {
        preventDefault: () => {}
    });
    expect(wrapper.state('error')).toBe('');
    expect(onSubmitSpy).toHaveBeenLastCalledWith({
        description: defaultExpenses[1].description,
        amount: defaultExpenses[1].amount,
        note: defaultExpenses[1].note,
        createdAt: defaultExpenses[1].createdAt,
    });
    expect(wrapper).toMatchSnapshot();
    // onSubmitSpy('Vivek', 'Mumbai');
    // expect(onSubmitSpy).toHaveBeenCalledWith('Vivek', 'Mumbai');
});

// test('should set new date on date change', () => {
//     const now = moment();
//     const wrapper = shallow(<ExpenseForm />);
//     wrapper.find('SingleDatePicker').prop('onDateChange')(now);
//     expect(wrapper.state('createdAt')).toEqual(now);
// });

// test('should set calendar focus on change', () => {
//     const focused = true;
//     const wrapper = shallow(<ExpenseForm />);
//     wrapper.find('SingleDatePicker').prop('onFocusChange')({ focused });
//     expect(wrapper.state('calendarFocused')).toBe(focused);
// });