import React from 'react';
import { shallow } from 'enzyme';

import ExpenseListItem from '../../components/ExpenseListItem';
import defaultExpenses from '../fixtures/expenses';

test('should render ExpenseListItem with passed expense', () => {
    const wrapper = shallow(<ExpenseListItem {...defaultExpenses[0]} />);
    expect(wrapper).toMatchSnapshot();
});