import moment from 'moment';
import selectExpenses, { getExpensesTotal } from '../../selectors/expenses';

const expenses = [
    {
        id: 1,
        description: 'Gum',
        note: '',
        amount: 195,
        createdAt: moment(0),
    }, {
        id: 2,
        description: 'Rent',
        note: '',
        amount: 150000,
        createdAt: moment(0).subtract(4, 'days'),
    }, {
        id: 3,
        description: 'Credit Card',
        note: '',
        amount: 4500,
        createdAt: moment(0).add(4, 'days'),
    }
]

test('should filter by text value', () => {
    const filters = {
        text: 'e',
        sortBy: 'date',
        startDate: undefined,
        endDate: undefined,
    };
    const result = selectExpenses(expenses, filters)
    expect(result).toEqual([ expenses[2], expenses[1] ])
})

test('should filter by start date', () => {
    const filters = {
        text: '',
        sortBy: 'date',
        startDate: moment(0),
        endDate: undefined,
    };
    const result = selectExpenses(expenses, filters)
    expect(result).toEqual([ expenses[2], expenses[0] ])
})

test('should filter by end date', () => {
    const filters = {
        text: '',
        sortBy: 'date',
        startDate: undefined,
        endDate: moment(0).add(1, 'year'),
    };
    const result = selectExpenses(expenses, filters)
    expect(result).toEqual([ expenses[2], expenses[0], expenses[1] ])
})

test('should sort by date', () => {
    const filters = {
        text: '',
        sortBy: 'date',
        startDate: undefined,
        endDate: undefined,
    };
    const result = selectExpenses(expenses, filters)
    expect(result).toEqual([ expenses[2], expenses[0], expenses[1] ])
})

test('should sort by amount', () => {
    const filters = {
        text: '',
        sortBy: 'amount',
        startDate: undefined,
        endDate: undefined,
    };
    const result = selectExpenses(expenses, filters);
    expect(result).toEqual([ expenses[1], expenses[2], expenses[0] ]);
})

test('should return total of 0 when no expenses are passed', () => {
    const result = getExpensesTotal([]);
    expect(result).toEqual(0);
})

test('should return amount when one expense is passed', () => {
    const result = getExpensesTotal([expenses[1]]);
    expect(result).toEqual(expenses[1].amount);
})

test('should return total when many expenses are passed', () => {
    const result = getExpensesTotal(expenses);
    expect(result).toEqual(expenses[0].amount + expenses[1].amount + expenses[2].amount);
})