import uuid from "uuid";
import moment from "moment";

import database from "../firebase/firebase.js";
import { array } from "prop-types";

// Actions
// ADD_EXPENSE, REMOVE_EXPENSE, EDIT_EXPENSE
export const addExpense = expense => ({
	type: "ADD_EXPENSE",
	expense
});

export const startAddExpense = (expenseData = {}) => {
	return dispatch => {
		const {
			description = "",
			note = "",
			amount = 0,
			createdAt = moment().valueOf()
		} = expenseData;
		const expense = { description, note, amount, createdAt };
		return database
			.ref("expenses")
			.push(expense)
			.then(ref => {
				dispatch(
					addExpense({
						id: ref.key,
						...expense
					})
				);
			});
	};
};

export const removeExpense = ({ id }) => ({
	type: "REMOVE_EXPENSE",
	id
});

export const startRemoveExpense = ({ id } = {}) => {
	return dispatch => {
		return database
			.ref(`expenses/${id}`)
			.remove()
			.then(() => {
				dispatch(removeExpense({ id }));
			});
	};
};

export const editExpense = (id, updates) => ({
	type: "EDIT_EXPENSE",
	id,
	updates
});

export const startEditExpense = (id, updates) => {
	return dispatch => {
		return database
			.ref(`expenses/${id}`)
			.update(updates)
			.then(() => {
				return dispatch(editExpense(id, updates));
			});
	};
};

export const setExpenses = expenses => ({
	type: "SET_EXPENSES",
	expenses
});

// 1. fetch all data once, 2. parse data into array, 3. dispatch SET_EXPENSES
export const startSetExpenses = () => {
	return dispatch => {
		return database
			.ref("expenses")
			.once("value")
			.then(snapshot => {
				const expenses = [];

				snapshot.forEach(childSnapshot => {
					expenses.push({
						id: childSnapshot.key,
						...childSnapshot.val()
					});
				});

				dispatch(setExpenses(expenses));
			});
	};
};
