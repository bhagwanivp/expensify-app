// Object Destructuring

const person = {
    name: 'Vivek',
    age: 24,
    location: {
        city: 'Pune',
        temp: 24
    }
};

// const { name: firstName = 'Anonymous', age } = person; // default for name is Anonymous
// const { city, temp: temperature } = person.location;

// console.log(`${firstName} is ${age}.`);
// console.log(`It's ${temperature} degrees Celsius in ${city}.`);

// const book = {
//     title: 'Skill with People',
//     author: 'Les Giblin',
//     publisher: {
//         name: 'Penguin',
//         edition: 'unknown'
//     }
// }

// const { title, author } = book;
// const { name: publisherName = 'Self Published'} = book.publisher;

// console.log(publisherName);


// Array Destructuring

const address = ['101', 'Meera Apartments', 'Baner Pashan Link Road', 'Pune', '411021', 'Maharashtra', 'India'];

// const [house, building, street, city, pincode, state, country ] = address;
const [house, building, street, city, , , country = 'Asia' ] = address;  // skipped pincode and state

console.log(`You are in ${city}, ${country}`);

const item1 = ['Coffee (hot)','$2.00','2.50','2.75'];
const item2 = ['Coffee (iced)','$3.00','3.50','3.75'];

const [ itemName, ,mediumPrice, largePrice ] = item2;

console.log(`A medium ${itemName} costs ${mediumPrice}`);