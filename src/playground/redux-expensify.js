import { createStore, combineReducers } from 'redux';
import uuid from 'uuid';

// Actions
// ADD_EXPENSE, REMOVE_EXPENSE, EDIT_EXPENSE
const addExpense = (
    { description = '', note = '', amount = 0, createdAt = 0 } = {}
) => ({
    type: 'ADD_EXPENSE',
    expense: {
        id: uuid(),
        description,
        note,
        amount,
        createdAt
    }
});

const removeExpense = (
    { id }
) => ({
    type: 'REMOVE_EXPENSE',
    id
});

const editExpense = ( 
    id, updates 
) => ({
    type: 'EDIT_EXPENSE',
    id,
    updates
});

// SET_TEXT_FILTER
const setTextFilter = ( 
    text = ''
) => ({
    type: 'SET_TEXT_FILTER',
    text
});

// SORT_BY_DATE, SORT_BY_AMOUNT
const sortByDate = () => ({
    type: 'SORT_BY_DATE',
});

const sortByAmount = () => ({
    type: 'SORT_BY_AMOUNT',
});

// SET_START_DATE, SET_END_DATE
const setStartDate = (startDate) => ({
    type: 'SET_START_DATE',
    startDate
});

const setEndDate = (endDate) => ({
    type: 'SET_END_DATE',
    endDate
});

// Expenses Reducer
const expensesReducerDefaultState = [];

const expensesReducer = ( state = expensesReducerDefaultState, action ) => {
    switch(action.type) {
        case 'ADD_EXPENSE':
            // return state.concat(action.expense);
            return [ ...state, action.expense ];    // spread operator to insert new expense
        case 'REMOVE_EXPENSE':
            return state.filter( ({ id }) => id !== action.id );
        case 'EDIT_EXPENSE':
            return state.map((expense) => {
                if(expense.id === action.id) {
                    return {
                        ...expense,
                        ...action.updates
                    }
                }
            });
        default: return state;
    }
};

// Filters Reducer
    const filtersReducerDefaultState = {
    text: '',
    sortBy: 'date',
    startDate: undefined,
    endDate: undefined
};

const filtersReducer = ( state = filtersReducerDefaultState, action ) => {
    switch ( action.type ) {
        case 'SET_TEXT_FILTER': return {
            ...state, 
            text: action.text
        };
        case 'SORT_BY_DATE': return {
            ...state,
            sortBy: 'date'
        };
        case 'SORT_BY_AMOUNT': return {
            ...state,
            sortBy: 'amount'
        };
        case 'SET_START_DATE': return {
            ...state,
            startDate: action.startDate
        };
        case 'SET_END_DATE': return {
            ...state,
            endDate: action.endDate
        };
        default: return state;
    }
};

// get all filtered and sorted expenses
const getVisibleExpenses = (expenses, { text, sortBy, startDate, endDate }) => {
    return expenses.filter((expense) => {
        const startDateMatch = typeof startDate !== 'number' || expense.createdAt >= startDate;
        const endDateMatch = typeof endDate !== 'number' || expense.createdAt <= endDate;
        const textMatch = expense.description.toLowerCase().includes(text.toLowerCase());
        return startDateMatch && endDateMatch && textMatch;
    }).sort((a, b) => {
        if (sortBy === 'date') {
            return a.createdAt < b.createdAt ? 1 : -1;
        }
        else if (sortBy === 'amount') {
            return a.amount < b.amount ? 1 : -1;
        }
    });
};

// Store Creation
const store = createStore(
    combineReducers({
        expenses: expensesReducer,
        filters: filtersReducer
    })
);

const unsubscribe = store.subscribe(() => {
    const state = store.getState();
    const visibleExpenses = getVisibleExpenses(state.expenses, state.filters);
    // console.log(store.getState());
    console.log(visibleExpenses);
});

const expenseOne = store.dispatch( addExpense({ description: 'rent', amount: 15000, createdAt: 200 }) );
const expenseTwo = store.dispatch( addExpense({ description: 'coffee', amount: 500, createdAt: -100 }) );
const expenseThree = store.dispatch( addExpense({ description: 'chocolate', amount: 800, createdAt: 300 }) );


// store.dispatch( removeExpense( { id: expenseTwo.expense.id } ) );

// store.dispatch( editExpense(expenseOne.expense.id, { amount: 800 }) );

// store.dispatch( setTextFilter('rent'));
// store.dispatch( setTextFilter() );

// store.dispatch( sortByAmount() );
// store.dispatch( sortByDate() );

// store.dispatch( setStartDate(125) );
// store.dispatch( setStartDate() );
// store.dispatch( setEndDate(250) );
// store.dispatch( setEndDate() );

const demoState = {
    expenses: [{
        id: 'abcdef',
        description: 'January Rent',
        note: 'This was the final payment for the address',
        amount: 54500,
        createdAt: 0
    }],
    filters: {
        text: 'rent',
        sortBy: 'amount', // date or amount
        startDate: undefined,
        endDate: undefined
    }
};