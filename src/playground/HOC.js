// Higher Order Components (HOC) - A component that renders another component
// Advantages - Reuse code, Render hijacking, Prop manipulation, Abstract state

import React from 'react';
import ReactDOM from 'react-dom';

const Info = (props) => (
    <div>
        <h1>Info</h1>
        <p>{props.info}</p>
    </div>
);

const withAdminWarning = (WrappedComponent) => {
    return (props) => (
        <div>
            { props.isAdmin && <p>This is private info. Please don't share.</p> }
            <WrappedComponent {...props} />
        </div>
    );
};

const requireAuthentication = (WrappedComponent) => {
    return (props) => (
        <div>
            { props.isAuthenticated 
                ? <WrappedComponent {...props} /> 
                : <p>Please log in to view this info.</p>
            }
        </div>
    );
};


const AdminInfo = withAdminWarning(Info);
const AuthInfo = requireAuthentication(Info);

const appRoot = document.getElementById('app');
// ReactDOM.render(<AdminInfo isAdmin={true} info='prop info here' />, appRoot);
ReactDOM.render(<AuthInfo isAuthenticated={true} info='prop info here' />, appRoot);