import React from "react";
import { NavLink } from "react-router-dom";

class Header extends React.Component {
	render() {
		return (
			<header>
				<h2>
					Expensify{" "}
					<span className="sub-heading">Track Your Expenses</span>
				</h2>
				<div className="nav-right">
					<NavLink to="/" activeClassName="is-active" exact>
						Dashboard
					</NavLink>
					<NavLink to="/create" activeClassName="is-active">
						Add Expenses
					</NavLink>
					{/* <NavLink to='/edit' activeClassName='is-active' >Edit Expenses</NavLink> */}
					<NavLink to="/help" activeClassName="is-active">
						Help
					</NavLink>
				</div>
			</header>
		);
	}
}

export default Header;
