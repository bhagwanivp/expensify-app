import React from 'react';
import ExpenseList from '../components/ExpenseList';

class Dashboard extends React.Component {
    render() {
        return (
        <div>
            <h2>Dashboard</h2>
            <ExpenseList />
        </div>
        );
    }
}

export default Dashboard;