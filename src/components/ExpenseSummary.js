import React from 'react';
import numeral from 'numeral';

import { getExpensesTotal } from '../selectors/expenses';

const ExpenseSummary = ({expenses, allExpensesCount}) => {
    let expensesTotal = numeral(getExpensesTotal(expenses)/100).format('$0,0.00');
    return (
        <p className="expenseListFilterInfo">
            Showing {expenses.length} out of {allExpensesCount} expense{allExpensesCount === 1 ? '':'s'} totaling {expensesTotal} as per your filters
        </p>
    );
}

export default ExpenseSummary;