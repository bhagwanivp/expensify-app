import React from 'react';
import { connect } from 'react-redux';

import selectExpenses from '../selectors/expenses';
import ExpenseListFilters from '../components/ExpenseListFilters';
import ExpenseListItem from './ExpenseListItem';
import ExpenseSummary from './ExpenseSummary';

export const ExpenseList = (props) => {
    let showExpenses = props.expenses.length > 0 || (props.filters.text || props.filters.startDate || props.filters.endDate);
    return (
    <div className="expenseList">
        <ExpenseListFilters />
        {
            showExpenses && <ExpenseSummary {...props} />
        }
        {
            showExpenses
            ? props.expenses.map((expense) => {
                return <ExpenseListItem key={expense.id} {...expense} />
              })
            : <p>No expenses found. Start by adding some.</p>
        }
    </div>
    )
};

const mapStateToProps = (state) => {
    return {
        allExpensesCount: state.expenses.length,
        expenses: selectExpenses(state.expenses, state.filters),
        filters: state.filters
    };
};

export default connect(mapStateToProps)(ExpenseList);