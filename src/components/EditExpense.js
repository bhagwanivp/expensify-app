import React from "react";
import { connect } from "react-redux";

import ExpenseForm from "./ExpenseForm";
import { startEditExpense, startRemoveExpense } from "../actions/expenses";

const _redirectToHome = props => {
	props.history.push("/");
};

export class EditExpense extends React.Component {
	onSubmit = expense => {
		this.props.startEditExpense(this.props.expense.id, expense);
		_redirectToHome(this.props);
	};
	onRemove = () => {
		this.props.startRemoveExpense({ id: this.props.expense.id });
		_redirectToHome(this.props);
	};

	render() {
		return (
			<div>
				<h2>Edit Expense ID: {this.props.expense.id}</h2>
				<ExpenseForm
					expense={this.props.expense}
					onSubmit={this.onSubmit}
				/>
				{this.props.expense && this.props.expense.id && (
					<div className="input-container">
						<button onClick={this.onRemove}>Remove</button>
					</div>
				)}
			</div>
		);
	}
}

const mapStateToProps = (state, props) => {
	return {
		expense: state.expenses.find(
			expense => expense.id === props.match.params.id
		)
	};
};

const mapDispatchToProps = dispatch => {
	return {
		startEditExpense: (id, expense) =>
			dispatch(startEditExpense(id, expense)),
		startRemoveExpense: ({ id }) => dispatch(startRemoveExpense({ id }))
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(EditExpense);
