import React from 'react';
import moment from 'moment';

import { SingleDatePicker } from 'react-dates';

class ExpenseForm extends React.Component {
    constructor(props) {
        super(props);
        if(props.expense) {
            let { description, amount, createdAt, note } = props.expense;
            this.state = {
                description,
                amount: (amount/100).toString(),
                createdAt: moment(createdAt),
                note,
                calendarFocused: false,
                error: ''
            };
        } else {
            this.state = {
                description: '',
                amount: '',
                createdAt: moment(),
                note: '',
                calendarFocused: false,
                error: ''
            };
        }
    }
    onDescriptionChange = (e) => {
        const description = e.target.value;
        this.setState( () => ({ description }) );
    };
    onAmountChange = (e) => {
        const amount = e.target.value;
        if( !amount || amount.match(/^\d{1,}(\.\d{0,2})?$/) ) {
            this.setState( () => ({ amount }) ); 
        }
    };
    onDateChange = (createdAt) => {
        if(createdAt) {
            this.setState( () => ({ createdAt }) );
        }
    }
    onNoteChange = (e) => {
        const note = e.target.value;
        this.setState( () => ({ note }) );
    };
    onFocusChange = ({ focused }) => {
        this.setState( () => ({ calendarFocused: focused }) )
    }
    onFormSubmit = (e) => {
        e.preventDefault();

        if(!this.state.description || !this.state.amount) {
            this.setState({ error: 'Please provide the description and amount' })
        } else {
            this.setState({ error: '' })
            this.props.onSubmit({
                description: this.state.description,
                amount: parseFloat(this.state.amount, 10) * 100,
                createdAt: this.state.createdAt.valueOf(),
                note: this.state.note,
            })
        }
    }

    render() {
        return (
            <div className="expenseForm">
                { this.state.error && <p className='error-message' >{this.state.error}</p> }
                <form onSubmit={this.onFormSubmit} >
                    <div className='input-container' >
                        <input 
                            type="text" 
                            placeholder="Short Description" 
                            autoFocus
                            value={this.state.description}
                            onChange={this.onDescriptionChange}
                        />
                    </div>
                    <div className='input-container' >
                        <input 
                            type="text" 
                            placeholder="Amount" 
                            value={this.state.amount}
                            onChange={this.onAmountChange}
                        />
                    </div>
                    <div className='input-container' >
                        <SingleDatePicker
                            date={this.state.createdAt}
                            onDateChange={this.onDateChange}
                            focused={this.state.calendarFocused}
                            onFocusChange={this.onFocusChange}
                            numberOfMonths={1}
                            isOutsideRange={() => false}
                        />
                    </div>
                    <div className='input-container' >
                        <textarea 
                            placeholder="Add a note for your expense (optional)" 
                            value={this.state.note}
                            onChange={this.onNoteChange} 
                            // cols='50'   
                        />
                    </div>
                    <div className='input-container' >
                        <button >Save Expense</button>
                    </div>
                </form>
            </div>
        )
    }    
}

export default ExpenseForm;