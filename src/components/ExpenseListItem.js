import React from 'react';
import moment from 'moment';
import numeral from 'numeral';
import { Link } from 'react-router-dom';

const  ExpenseListItem = ({ description, amount, createdAt, id }) => (
    <div className="expenseListItem">
        <p>
            {description} &mdash; {numeral(amount/100).format('$0,0.00')} &mdash; {moment(createdAt).format('Do MMM YYYY')}
        </p>
        <Link to={'/edit/'+id} ><button>Edit</button></Link>
    </div>
);

export default ExpenseListItem;