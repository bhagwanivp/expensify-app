const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

let env = process.env.NODE_ENV || 'development';
console.log('\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\');
console.log('NODE_ENV set to', process.env.NODE_ENV);
console.log('Webpack environment set to', env);
console.log('\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\');
let isProduction = env === 'production';
let isDevelopment = env === 'development';
let isTest = env === 'test';

if(isTest) {
    require('dotenv').config({ path: '.env.test'});
} else if(isDevelopment) {
    require('dotenv').config({ path: '.env.development'});
}

const CSSExtract = new ExtractTextPlugin('styles.css');

let genericPlugins = [
    CSSExtract,
    new webpack.DefinePlugin({
        'process.env.FIREBASE_API_KEY': JSON.stringify(process.env.FIREBASE_API_KEY),
        'process.env.FIREBASE_AUTH_DOMAIN': JSON.stringify(process.env.FIREBASE_AUTH_DOMAIN),
        'process.env.FIREBASE_DATABASE_URL': JSON.stringify(process.env.FIREBASE_DATABASE_URL),
        'process.env.FIREBASE_PROJECT_ID': JSON.stringify(process.env.FIREBASE_PROJECT_ID),
        'process.env.FIREBASE_STORAGE_BUCKET': JSON.stringify(process.env.FIREBASE_STORAGE_BUCKET),
        'process.env.FIREBASE_MESSAGING_SENDER_ID': JSON.stringify(process.env.FIREBASE_MESSAGING_SENDER_ID),
    })
];

let devPlugins = [
    new webpack.NamedModulesPlugin(),
    new webpack.NamedChunksPlugin(),
];


let config = {
    resolve: {
        alias: isDevelopment ? {
          'react-dom': '@hot-loader/react-dom'
        } : {}
    },
    entry: isDevelopment ? [
        'react-hot-loader/patch',   // Hot Module Replacement (HMR)
        './src/app.js',             // app entry point
    ] : ['./src/app.js'],
    output: {
        path: path.join(__dirname, 'public', 'dist'),
        filename: 'bundle.js'
    },
    mode: env,
    module: {
        rules: [{
            test: /\.js$/,
            loader: isDevelopment ? [
                'babel-loader',
                "react-hot-loader/webpack",
            ] : ['babel-loader'],
            exclude: /node_modules/
        }, {
            test: /\.s?css$/,
            use: CSSExtract.extract({
                use: [
                    // 'style-loader',
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true,
                        }
                    }, 
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true,
                        }
                    }
                ]
            })
        }]
    },
    plugins: isDevelopment ? [...genericPlugins, ...devPlugins] : [...genericPlugins],
    devtool: isProduction ? 'source-map' : 'inline-source-map',
    devServer: {
        contentBase: path.join(__dirname, 'public'),
        publicPath: '/dist/',
        historyApiFallback: true,
        // https: true,
        // hot: true,
        // inline: true,
        open: true,
    }
};

module.exports = (/* env, args */) => { 
    // console.log(env, args);
    return config;
}
