# expensify-app

About the App:
It's an Expense Management App for individual use.
It's built with ReactJS and Redux, served with Express, tested with Jest and Enzyme, bundled with Webpack, packaged with NPM and Yarn, and version-controlled with Gitlab.
See it deployed on Heroku here: https://expensify-react-course-app.herokuapp.com/

Features:
* Add new expenses having date, amount, description, special notes etc.
* Sort and Filter through your expenses on the Dashboard based on date, amount, description etc.
* Edit existing expenses
* Delete existing expenses

Get started:
1. Clone the Repository
2. In the root directory of the repo - 
    To create and serve the production build, run 'npm run demo'
    OR
    To start the development server, run 'npm run dev'
3. To execute tests, run 'npm test'
4. To start the continuous test watcher for Test Driven Development, 
    run 'npm run tdd' on a new terminal in the same root directory.

It is a React Udemy Course Project with the following improvements:
- added React Hot Loader (automatically re-renders when code is changed)
- added React Hot Loader's patch for React 16.8+ 
- upgraded Babel and its plugins, presets etc. to v7
- added file watcher for Test Driven Development

TO DOs:
- clean up store manipulation code from configureStore
- find a solution for DatePicker unit testing issue (unable to find the component)